import { IResolvers } from "graphql-tools";
import { Usuario } from "./../Models/User";
import { Rol } from "./../Models/Rol";

var users: Usuario[] = [{
    id: "1",
    nombre: "Henry",
    apellido: "Ruiz",
    email: "armandy2001@gmail.com",
    cedula: "0929992832",
    telefono: "0992249693",
    rol: {
        id: "1",
        nombre: "Administrador"
    }
},{
    id: "2",
    nombre: "Tiffany",
    apellido: "Cruz",
    email: "tifftamm@gmail.com",
    cedula: "0929992833",
    telefono: "0992249694",
    rol: {
        id: "2",
        nombre: "Usuario"
    }
},
{
    id: "3",
    nombre: "Josthin",
    apellido: "Ayon",
    email: "jayon@gmail.com",
    cedula: "0929992834",
    telefono: "0992249695",
    rol: {
        id: "2",
        nombre: "Usuario"
    }
}]

var rol : Rol[] = [{
    id: "1",
    nombre: "Administrador"
},{
    id: "2",
    nombre: "Usuario"
}];

export const UsuarioResolver: IResolvers = {
    Query: {
        Usuarios: () => {
            return users;
        },
        user: (root,args) => {
            const { id } = args;
            return users.find(user => user.id === id);
        },
        countUsuarios: () => {
            return users.length;
        }
    },
    Mutation: {
        createUsuario: (root,args) => {
            const { nombre, apellido, email, cedula, telefono, idRol } = args;
            const foundRol = rol.find((rol: { id: any; }) => rol.id === idRol);
            if (!foundRol) {
                throw new Error("Invalid rol ID");
            }
            const newUsuario: Usuario = {
                id: String(users.length + 1),
                nombre,
                apellido,
                email,
                cedula,
                telefono,
                rol: foundRol
            }
            users.push(newUsuario);
            return newUsuario;
        },
    }
};
