import { IResolvers } from "graphql-tools";
import { Rol } from "./../Models/Rol";

var rol : Rol[] = [{
    id: "1",
    nombre: "Administrador"
},{
    id: "2",
    nombre: "Usuario"
}];

export function getRol(id: string): Rol{
    return rol.find(rol => rol.id === id) as Rol;
}

export const RolResolver: IResolvers = {
    Query: {
        Roles: () => {
            return rol;
        },
        rol: (root,args) => {
            const { id } = args;
            return rol.find(rol => rol.id === id);
        },
        countRoles: () => {
            return rol.length;
        }
    },
    Mutation: {
        createRol: (root,args) => {
            const { nombre } = args;
            const newRol: Rol = {
                id: String(rol.length + 1),
                nombre
            }
            rol.push(newRol);
            return newRol;
        }
    }
};
