import { IResolvers } from "graphql-tools";
import { Rol } from "../Models/Rol";
import dataSource from "../Configs/ormconfig";

const rolRepository = dataSource.getRepository(Rol);

export const RolResolver: IResolvers = {
  Query: {
    Roles: async (): Promise<Rol[]> => {
      try {
        const roles = await rolRepository.find();
        return roles;
      } catch (error) {
        console.error(error);
        return [];
      }
    },
    rol: async (_, { id }): Promise<Rol | undefined> => {
      try {
        const rol = await rolRepository.findOne({ where: { id } });
        return rol || undefined;
      } catch (error) {
        console.error(error);
        return undefined;
      }
    },
    countRoles: async (): Promise<number> => {
      try {
        const count = await rolRepository.count();
        return count;
      } catch (error) {
        console.error(error);
        return 0;
      }
    },
  },
  Mutation: {
    createRol: async (_, args): Promise<Rol> => {
      try {
        const { nombre } = args;

        const id = Math.random().toString(36).substr(2, 9);

        const newRol = new Rol(id, nombre );

        await rolRepository.save(newRol);
        return newRol;
      } catch (error) {
        console.error(error);
        return [] as any;
      }
    },
  },
};
