import { IResolvers } from "graphql-tools";
import { Usuario } from "../Models/User";
import { Rol } from "../Models/Rol";
import dataSource from "../Configs/ormconfig";

const userRepository= dataSource.getRepository(Usuario);
const rolRepository= dataSource.getRepository(Rol);

export const UsuarioResolver: IResolvers = {
  Query: {
    Usuarios: async (): Promise<Usuario[]> => {
      try {
        const users = await userRepository.find({ relations: ['rol'] }); 
        return users;
      } catch (error) {
        console.error(error);
        return [] as any;
      }
    },
    user: async (_, { id }): Promise<Usuario | undefined> => {
      try {
        const user = await userRepository.findOne({
          where: { id },
          relations: ["rol"],
        });
        return user || undefined;
      } catch (error) {
        console.error(error);
        return [] as any;
      }
    },
    countUsuarios: async (): Promise<number> => {
      try {
        const count = await userRepository.count();
        return count;
      } catch (error) {
        console.error(error);
        return [] as any;
      }
    },
  },
  Mutation: {
    createUser: async (_, args): Promise<Usuario> => {
      try {
        const { nombre, apellido, email, cedula, telefono, idRol } = args;

        const foundRol = await rolRepository.findOne({ where: { id: idRol } });
        if (!foundRol) {
          throw new Error("Invalid rol ID");
        }
        const id = Math.random().toString(36).substr(2, 9);

        const newUser = new Usuario(id, nombre, apellido, email, cedula, telefono, foundRol);

        await userRepository.save(newUser);
        return newUser;
      } catch (error) {
        console.error(error);
        return [] as any;
      }
    },
  },
};
