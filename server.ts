import { ApolloServer, gql } from "apollo-server";
import { RolResolver } from "./Resolvers/Rol.resolver";
import { UsuarioResolver } from "./Resolvers/Usuario.resolver";
import { dataSource } from "./Configs/ormconfig";

const typeDefs = gql`
  type Rol {
    id: Int
    nombre: String
  }

  type Usuario {
    id: Int
    nombre: String
    apellido: String
    email: String
    cedula: String
    telefono: String
    rol: Rol
  }

  type Query {
    Usuarios: [Usuario]
    user(id: String!): Usuario
    countUsuarios: Int!
    Roles: [Rol]
    rol(id: String!): Rol!
    countRoles: Int!
  }

  type Mutation {
    createUser(
      nombre: String!
      apellido: String!
      email: String!
      cedula: String!
      telefono: String!
      idRol: String!
    ): Usuario!
    createRol(nombre: String!): Rol!
  }
`;

console.log("Conetando con la base de datos...");
dataSource
  .initialize()
  .then(() => console.log("Base de datos conectada"))
  .catch((err) => console.error("Error al conectar a la base de datos: ", err));
dataSource.initialize();

const server = new ApolloServer({
  typeDefs,
  resolvers: [UsuarioResolver, RolResolver],
  context: () => ({ dataSource }),
  
});


server.listen().then(({ url }) => {
  console.log(dataSource.options.entities);
  console.log(`El Sandbox está corriendo en  ${url}`);
});
