import { DataSource } from "typeorm";
import { Usuario } from "../Models/User";
import { Rol } from "../Models/Rol";

export const dataSource = new DataSource({
  type: "postgres",
  host: "localhost",
  port: 5432,
  username: "postgres",
  password: "12345",
  database: "postgres",
  entities: [Rol, Usuario],
  synchronize: true,
  schema: "public",
});

export default dataSource;
