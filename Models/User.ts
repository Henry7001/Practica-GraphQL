import { Entity, PrimaryGeneratedColumn, Column, ManyToOne } from 'typeorm';
import { Rol } from "./Rol";

@Entity("usuario")
export class Usuario {
  @PrimaryGeneratedColumn()
  id: string;
  @Column()
  nombre: string;
  @Column()
  apellido: string;
  @Column()
  email: string;
  @Column()
  cedula: string;
  @Column()
  telefono: string;
  @ManyToOne(() => Rol, (rol) => rol.id) 
  rol: Rol;
  constructor(id: string, nombre: string, apellido: string, email: string, cedula: string, telefono: string, rol: Rol) {
    this.id = id;
    this.nombre = nombre;
    this.apellido = apellido;
    this.email = email;
    this.cedula = cedula;
    this.telefono = telefono;
    this.rol = rol;
  }
}
