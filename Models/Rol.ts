import { Entity, PrimaryGeneratedColumn, Column } from 'typeorm';

@Entity("rol")
export class Rol{
    @PrimaryGeneratedColumn()
    id: string;
    @Column()
    nombre: string;
    constructor(id: string, nombre: string){
        this.id = id;
        this.nombre = nombre;
    }
}